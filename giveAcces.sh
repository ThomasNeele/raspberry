#!/bin/bash

chmod +x /home/pi/raspberry/slidehub.sh
chmod +x /home/pi/raspberry/power.sh
chmod +x /home/pi/raspberry/registerPi.py
chmod +x /home/pi/raspberry/on.sh
chmod +x /home/pi/raspberry/off.sh
chmod +x /home/pi/raspberry/initPi.sh
chmod +x /home/pi/raspberry/createPi.py
chmod +x /home/pi/raspberry/keys.sh
chmod +x /home/pi/raspberry/resettunnel.sh
