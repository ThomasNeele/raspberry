# Instellen test of live

Pas de environment variabele aan in slidehub.config
Geldige waardes zijn 'test' of 'live'

# stappenplan instalatie


1. installeer de raspberry pi os op een sd kaart doormiddel van rpi imager

2. volg de installatie bij het opstarten van de pi en zorg ervoor dat de gebruikersnaam pi is

3. zet de raspberry repository op de raspberry pi onder de path /home/pi/

4. maak initPi.sh executable met de command `chmod +x initPi.sh`

5. run initPi.sh met de command `./initPi.sh` in de path /home/pi/raspberry

6. zet de public key van de pi op de server in de paths /.ssh/authorized_keys en /opt/tomcat/.ssh/authorized_keys

# Raspberry pi tunnels

voor een directe verbinding tussen de pi's en de server word er een ssh tunnel gemaakt
deze tunnel werkt als volgt

* vanaf de pi word een tunnel gemaakt op een vooraf bepaalde poort.
  zodra deze tunnel is gemaakt kan de server deze gebruiken om met de pi te verbinden.
  de server stuurt ssh commands naar de pi om bijvoorbeeld
  het scherm aan of uit te zetten.

## maken van de tunnel
de tunnel moet eerst goed geconfigureerd worden. dit gebeurd met een python bestand
 
* via het python script word een api call gedaan naar de slidehub server
  in de response van deze call zit een poort nummer. zodra python dit nummer binnenkrijgt gaat hij in 

  `/etc/systemd/system/` zoeken naar `slidehubtunnel.service` als deze file nog niet geconfigureerd is staat hier een PORT in
  python vervangt dit automatisch met het goede poort nummer voor de pi 

* zodra het goeie poort nummer er staat zal python het commando ``systemctl daemon-reload`` en vervolgens enabled hij de service en start hem meteen
  enable zorgt ervoor dat de service word gestart bij het opstarten van de pi
  dus zodra python heeft gerund zal de tunnel altijd automatisch gemaakt worden

* zodra de tunnel draait kan je vanaf de server verbinden met het commando 
  ``ssh -l pi -p POORT localhost`` en kunnen normale ssh commands uitgevoerd worden

## wat als de tunnel niet werkt

### check error

als de tunnel niet werkt kijk naar het probleem dmv ``systemctl status slidehub.tunnel`` Hier zal een foutcode bij staan

