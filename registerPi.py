#!/usr/bin/python3


import requests
import os
from configparser import ConfigParser

parser = ConfigParser()
with open("slidehub.config") as stream:
    parser.read_string("[top]\n" + stream.read())



id = os.popen("cat /proc/cpuinfo | grep Serial | cut -d ' ' -f 2").read()

register_url = ""
if parser["top"]["environment"] == "testing":
    register_url = parser["top"]["testing"]
elif parser["top"]["environment"] == "live":
    register_url = parser["top"]["live"]

print('hoi' + register_url)

id = os.popen("cat /proc/cpuinfo | grep Serial | cut -d ' ' -f 2").read()
register_url += f'/registerPi?serialID={id}'
response = requests.get(register_url.strip())
if response.status_code == 200:
  port = response.json()['poort']
  print(port)
  os.system('sudo systemctl stop slidehubtunnel.service')
  os.system('sudo systemctl disable slidehubtunnel.service')
  os.system('sudo cp slidehubtunnel.service /etc/systemd/system/slidehubtunnel.service')
  command = f"sudo sed -i 's/PORT/{port}/g' /etc/systemd/system/slidehubtunnel.service"
  os.system(command)
  os.system('sudo systemctl daemon-reload')
  os.system('sudo systemctl enable slidehubtunnel.service')
  os.system('sudo systemctl start slidehubtunnel.service')
