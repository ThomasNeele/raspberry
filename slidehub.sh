#!/bin/bash
id="$(cat /proc/cpuinfo | grep Serial | cut -d ' ' -f 2)"
ip="$(hostname -I)"
source /home/pi/raspberry/slidehub.config
url=""
if [ $environment = "testing" ]
	then 
		url=$testing
	elif [ $environment = "live" ]
	then
		url=$live
	else
		echo Invalid config
fi
url+="/piIp?serialID=$id&ip=$ip"
echo $url
xset s noblank
xset s off
xset -dpms

rm ~/.cache/chromium -rf
chromium-browser $url --disable-features=Translate  --autoplay-policy=no-user-gesture-required --kiosk
