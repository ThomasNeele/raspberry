#!/bin/bash
sudo apt update -y && sudo apt upgrade -y
sudo systemctl enable ssh
sudo sed -i /etc/ssh/ssh_config -e 's/StrictHostKeyChecking ask/StrictHostKeyChecking no/g' /etc/ssh/ssh_config
sudo cp ~/raspberry/autostart /etc/xdg/lxsession/LXDE-pi/autostart
sudo cp /home/pi/raspberry/slidehubtunnel.service /etc/systemd/system/slidehubtunnel.service
(crontab -l 2>/dev/null; echo "0 0 * * * sudo reboot") | crontab -
sudo apt install cec-utils -y
sudo mv ~/raspberry/unclutter /opt
sudo ln -s /opt/unclutter /usr/bin/unclutter
sh /home/pi/raspberry/giveAcces.sh
/home/pi/raspberry/keys.sh
/home/pi/raspberry/createPi.py
echo "regestering pi"
sleep 15s
/home/pi/raspberry/registerPi.py
echo 'on 0' | cec-client -s -d 1
sudo reboot
