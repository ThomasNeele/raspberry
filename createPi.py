#!/usr/bin/python3

import requests
import os
from configparser import ConfigParser

parser = ConfigParser()
with open("slidehub.config") as stream:
    parser.read_string("[top]\n" + stream.read())


id = os.popen("cat /proc/cpuinfo | grep Serial | cut -d ' ' -f 2").read()

register_url = ""
if parser["top"]["environment"] == "testing":
    register_url = parser["top"]["testing"]
elif parser["top"]["environment"] == "live":
    register_url = parser["top"]["live"]

register_url += f"/piIp?serialID={id}"
requests.post(register_url.strip(), timeout=3)
